package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int rigthSpace = (width - text.length());
			return " ".repeat(rigthSpace) + text;

		}

		public String flushLeft(String text, int width) {
			int leftSpace = (width - text.length());
			return text + " ".repeat(leftSpace);
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
	// @Test
	// void test() {
	// 	fail("Not yet implemented");
	
	// }

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("    ", aligner.center("",5));
	}

	@Test 
	void flushRight() {
		assertEquals("  foo", aligner.flushRight("foo", 5));
	}

	@Test
	void flushLeft() {
		assertEquals("foo  ", aligner.flushLeft("foo", 5));
	}
		

		
		



// text = null
// text er en tom string
// text lengre enn en linje
// text har ikke mellomrom

	
}
